package eu.softmark.gitbrowser.model.bitbucket

import eu.softmark.gitbrowser.model.RepositoryItem


class BitbucketRepositoryItem(name: String,
                              username: String,
                              userAvatar: String,
                              description: String?)
    : RepositoryItem(name, username, userAvatar, description)