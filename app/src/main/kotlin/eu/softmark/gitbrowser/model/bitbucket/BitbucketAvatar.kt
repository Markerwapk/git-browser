package eu.softmark.gitbrowser.model.bitbucket

import com.google.gson.annotations.SerializedName

data class BitbucketAvatar(@SerializedName("href") val href: String)