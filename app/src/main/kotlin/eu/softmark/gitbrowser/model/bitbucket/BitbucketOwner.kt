package eu.softmark.gitbrowser.model.bitbucket

import com.google.gson.annotations.SerializedName

data class BitbucketOwner(@SerializedName("username") val username: String,
                          @SerializedName("display_name") val displayName: String,
                          @SerializedName("account_id") val accountId: String,
                          @SerializedName("links") val bitbucketLinks: BitbucketLinks,
                          @SerializedName("type") val type: String,
                          @SerializedName("uuid") val uuid: String)