package eu.softmark.gitbrowser.model.bitbucket

import com.google.gson.annotations.SerializedName

data class BitbucketValue(@SerializedName("owner") val bitbucketOwner: BitbucketOwner,
                          @SerializedName("name") val name: String,
                          @SerializedName("description") val description: String)