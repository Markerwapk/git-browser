package eu.softmark.gitbrowser.model.bitbucket

import com.google.gson.annotations.SerializedName


data class BitbucketRepository(@SerializedName("values") val bitbucketValues: List<BitbucketValue>)