package eu.softmark.gitbrowser.model.bitbucket

import com.google.gson.annotations.SerializedName

data class BitbucketLinks(@SerializedName("self") val bitbucketSelf: BitbucketSelf,
                          @SerializedName("html") val bitbucketHtml: BitbucketHtml,
                          @SerializedName("avatar") val bitbucketAvatar: BitbucketAvatar)