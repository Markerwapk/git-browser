package eu.softmark.gitbrowser.model.gitbub

import eu.softmark.gitbrowser.model.RepositoryItem


class GithubRepositoryItem(name: String,
                           username: String,
                           userAvatar: String,
                           description: String?)
    : RepositoryItem(name, username, userAvatar, description)