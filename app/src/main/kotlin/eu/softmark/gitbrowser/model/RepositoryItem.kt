package eu.softmark.gitbrowser.model

import java.io.Serializable


abstract class RepositoryItem(val name: String,
                     val username: String,
                     val userAvatar: String,
                     val description: String?) : Serializable