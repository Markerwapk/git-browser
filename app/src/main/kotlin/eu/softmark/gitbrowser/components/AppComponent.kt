package eu.softmark.gitbrowser.components

import dagger.Component
import eu.softmark.gitbrowser.activities.SplashScreenActivity
import eu.softmark.gitbrowser.activities.repository.details.RepositoryDetailsActivity
import eu.softmark.gitbrowser.activities.repository.list.RepositoriesActivity
import eu.softmark.gitbrowser.modules.*
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class), (ActivitiesModule::class), (ApiModule::class), (UtilsModule::class), (MapperModule::class)])
interface AppComponent {
    fun inject(repositoriesActivity: RepositoriesActivity)
    fun inject(repositoriesActivity: SplashScreenActivity)
    fun inject(repositoryDetailsActivity: RepositoryDetailsActivity)
}
