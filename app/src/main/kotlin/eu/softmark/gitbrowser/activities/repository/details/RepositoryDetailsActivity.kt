package eu.softmark.gitbrowser.activities.repository.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import eu.softmark.gitbrowser.App
import eu.softmark.gitbrowser.R
import eu.softmark.gitbrowser.activities.SplashScreenActivity
import eu.softmark.gitbrowser.model.RepositoryItem
import kotlinx.android.synthetic.main.repository_details_layout.*
import java.lang.Exception
import javax.inject.Inject


class RepositoryDetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var picasso: Picasso

    companion object {
        private const val REPOSITORY_ITEM = "repository_item"

        fun getIntentToStartActivity(context: Context, repositoryItem: RepositoryItem): Intent {
            val intent = Intent(context, RepositoryDetailsActivity::class.java)
            intent.putExtra(REPOSITORY_ITEM, repositoryItem)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
        setContentView(R.layout.repository_details_layout)

        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val repositoryItem = intent.extras.getSerializable(REPOSITORY_ITEM) as? RepositoryItem

        repositoryItem?.let {
            fillView(it)
        } ?: goToSplashScreen() //security, when activity create without repository item

    }

    private fun goToSplashScreen() {
        finish()
        startActivity(Intent(this, SplashScreenActivity::class.java))
    }

    private fun fillView(repositoryItem: RepositoryItem) {
        description.text = repositoryItem.description
        supportActionBar?.title = repositoryItem.name
        repositoryUsername.text = repositoryItem.username
        avatar.transitionName = repositoryItem.name

        picasso.load(repositoryItem.userAvatar).noFade().into(avatar, object : Callback {
            override fun onSuccess() {
                supportStartPostponedEnterTransition()
            }

            override fun onError(e: Exception?) {
                supportStartPostponedEnterTransition()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}