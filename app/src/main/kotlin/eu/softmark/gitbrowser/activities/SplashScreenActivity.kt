package eu.softmark.gitbrowser.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import eu.softmark.gitbrowser.App
import eu.softmark.gitbrowser.activities.repository.list.RepositoriesActivity
import javax.inject.Inject


class SplashScreenActivity : AppCompatActivity(), Runnable {
    companion object {

        private const val START_NEXT_ACTIVITY_DELAY = 2 * 1000L
    }

    @Inject
    lateinit var handler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.appComponent.inject(this)
    }

    override fun onStart() {
        super.onStart()
        handler.postDelayed(this, START_NEXT_ACTIVITY_DELAY)
    }

    override fun onStop() {
        handler.removeCallbacks(this)
        finish()
        super.onStop()
    }

    override fun run() {
        startActivity(Intent(this, RepositoriesActivity::class.java))
    }
}