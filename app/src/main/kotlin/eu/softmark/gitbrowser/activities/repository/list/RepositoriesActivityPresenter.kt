package eu.softmark.gitbrowser.activities.repository.list

import eu.softmark.gitbrowser.R
import eu.softmark.gitbrowser.utils.GitRepositoryCache
import java.io.IOException


class RepositoriesActivityPresenter(private val gitRepositoryCache: GitRepositoryCache) : GitRepositoryCache.GitRepositoryListener {

    companion object {
        private const val SORT_STATE_NONE = 0
        private const val SORT_STATE_ASC = 1
        private const val SORT_STATE_DESC = 2
    }

    private lateinit var view: RepositoriesActivityView
    private var sortState = SORT_STATE_NONE

    private var init = true

    fun onCreate(view: RepositoriesActivityView) {
        this.view = view
        view.prepareList()
    }

    fun onStart() {
        gitRepositoryCache.register(this)
        if (init) {
            init = false
            gitRepositoryCache.refreshData()
        }
    }

    fun onStop() {
        gitRepositoryCache.unregister(this)
    }

    override fun onRefreshing() {
        view.showList(listOf()) //clear list
        view.hideError()
        view.showProgress()
    }

    override fun onFinish() {
        sortState = SORT_STATE_NONE
        view.showList(gitRepositoryCache.repositoryItems)
        view.hideError()
        view.hideProgress()
    }

    override fun onError(throwable: Throwable) {
        when (throwable) {
            is IOException -> view.showError(R.string.internet_problem_error)
            else -> view.showError(R.string.other_error)
        }
        view.showList(listOf()) //clear list
        view.hideProgress()
    }

    fun refreshRepositories() {
        gitRepositoryCache.refreshData()
    }

    fun onSortClicked() {
        if (sortState == SORT_STATE_NONE || sortState == SORT_STATE_DESC) {
            sortState = SORT_STATE_ASC
            view.showList(gitRepositoryCache.repositoryItems.sortedBy { it.name.toLowerCase().trim() })
        } else {
            sortState = SORT_STATE_DESC
            view.showList(gitRepositoryCache.repositoryItems.sortedByDescending { it.name.toLowerCase().trim() })
        }
    }
}