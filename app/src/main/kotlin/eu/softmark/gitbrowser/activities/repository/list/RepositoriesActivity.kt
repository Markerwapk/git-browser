package eu.softmark.gitbrowser.activities.repository.list

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.transition.Slide
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import eu.softmark.gitbrowser.App
import eu.softmark.gitbrowser.R
import eu.softmark.gitbrowser.activities.repository.details.RepositoryDetailsActivity
import eu.softmark.gitbrowser.model.RepositoryItem
import kotlinx.android.synthetic.main.repositories_activity_layout.*
import javax.inject.Inject
import android.support.v4.util.Pair as UtilPair

class RepositoriesActivity : AppCompatActivity(), RepositoriesActivityView, RepositoriesAdapter.RepositoriesAdapterListener {

    @Inject
    lateinit var presenter: RepositoriesActivityPresenter

    @Inject
    lateinit var adapter: RepositoriesAdapter

    @OnClick(R.id.tryAgainButton)
    fun onErrorButtonClicked() {
        presenter.refreshRepositories()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(window) {
            requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
            exitTransition = Slide()
        }

        App.appComponent.inject(this)
        setContentView(R.layout.repositories_activity_layout)

        ButterKnife.bind(this)
        setSupportActionBar(toolbar)

        swipeToRefresh.setOnRefreshListener {
            presenter.refreshRepositories()
        }

        presenter.onCreate(this)
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onStop() {
        presenter.onStop()
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.repositories_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sort -> {
                presenter.onSortClicked()
                return true
            }
        }
        return false
    }

    override fun showProgress() {
        swipeToRefresh.isRefreshing = true
    }

    override fun hideProgress() {
        swipeToRefresh.isRefreshing = false
    }

    override fun showError(@StringRes errorRes: Int) {
        errorLabel.setText(errorRes)
        errorMask.visibility = View.VISIBLE
    }

    override fun hideError() {
        errorMask.visibility = View.GONE
    }

    override fun prepareList() {
        list.layoutManager = LinearLayoutManager(this)
        adapter.listener = this
        list.adapter = adapter
    }

    override fun showList(repositoryItems: List<RepositoryItem>) {
        adapter.items = repositoryItems
    }

    override fun onItemClicked(repositoryItem: RepositoryItem, avatar: ImageView) {
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                UtilPair.create(avatar, ViewCompat.getTransitionName(avatar)))

        startActivity(
                RepositoryDetailsActivity.getIntentToStartActivity(this, repositoryItem),
                options.toBundle())
    }
}