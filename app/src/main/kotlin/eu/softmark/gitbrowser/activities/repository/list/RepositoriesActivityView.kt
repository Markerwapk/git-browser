package eu.softmark.gitbrowser.activities.repository.list

import android.support.annotation.StringRes
import eu.softmark.gitbrowser.model.RepositoryItem


interface RepositoriesActivityView {
    fun showProgress()
    fun hideProgress()
    fun showError(@StringRes errorRes: Int)
    fun hideError()
    fun showList(repositoryItems: List<RepositoryItem>)
    fun prepareList()
}