package eu.softmark.gitbrowser.activities.repository.list

import android.support.annotation.CallSuper
import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.squareup.picasso.Picasso
import eu.softmark.gitbrowser.R
import eu.softmark.gitbrowser.model.RepositoryItem
import eu.softmark.gitbrowser.model.bitbucket.BitbucketRepositoryItem
import eu.softmark.gitbrowser.model.gitbub.GithubRepositoryItem


class RepositoriesAdapter(private val picasso: Picasso) : RecyclerView.Adapter<RepositoriesAdapter.RepositoryViewHolder>() {

    companion object {
        private const val TYPE_GITHUB = 0
        private const val TYPE_BITBUCKET = 1
    }

    interface RepositoriesAdapterListener {
        fun onItemClicked(repositoryItem: RepositoryItem, avatar: ImageView)
    }

    var listener: RepositoriesAdapterListener? = null

    var items: List<RepositoryItem> = listOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryViewHolder =
            when (viewType) {
                TYPE_GITHUB -> GithubRepositoryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.repository_view_holder_layout, parent, false))
                TYPE_BITBUCKET -> BitbucketRepositoryViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.repository_view_holder_layout, parent, false))
                else -> throw IllegalStateException("Type not found")
            }

    override fun getItemViewType(position: Int): Int = when (items[position]) {
        is BitbucketRepositoryItem -> TYPE_BITBUCKET
        is GithubRepositoryItem -> TYPE_GITHUB
        else -> throw IllegalStateException("Type not found")
    }

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(holder: RepositoryViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    abstract inner class RepositoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.repositoryName)
        lateinit var repositoryName: TextView

        @BindView(R.id.repositoryIcon)
        lateinit var repositoryIcon: ImageView

        @BindView(R.id.repositoryUsername)
        lateinit var repositoryUsername: TextView

        @BindView(R.id.avatar)
        lateinit var avatar: ImageView

        init {
            bindViews()
        }

        private fun bindViews() {
            ButterKnife.bind(this, itemView)
        }

        @CallSuper
        open fun bind(item: RepositoryItem) {
            repositoryName.text = item.name
            repositoryUsername.text = itemView.context.getString(R.string.username_value, item.username)

            itemView.setOnClickListener { listener?.onItemClicked(item, avatar) }

            picasso.load(item.userAvatar).placeholder(R.drawable.avatar_placeholder).into(avatar)

            ViewCompat.setTransitionName(avatar, item.name)
            ViewCompat.setTransitionName(repositoryName, item.name)
        }
    }

    inner class GithubRepositoryViewHolder(view: View) : RepositoryViewHolder(view) {

        override fun bind(item: RepositoryItem) {
            super.bind(item)
            repositoryIcon.setImageResource(R.drawable.ic_github)
        }
    }

    inner class BitbucketRepositoryViewHolder(view: View) : RepositoryViewHolder(view) {

        override fun bind(item: RepositoryItem) {
            super.bind(item)
            repositoryIcon.setImageResource(R.drawable.ic_bitbucket)
        }
    }
}