package eu.softmark.gitbrowser.webapi

import eu.softmark.gitbrowser.model.gitbub.GitHubRepository
import io.reactivex.Observable
import retrofit2.http.GET


interface GithubApi {

    companion object {
        const val BASE_URL = "https://api.github.com/"
    }

    @GET("repositories")
    fun getRepositories(): Observable<Array<GitHubRepository>>
}