package eu.softmark.gitbrowser.webapi

import eu.softmark.gitbrowser.model.bitbucket.BitbucketRepository
import io.reactivex.Observable
import retrofit2.http.GET


interface BitbucketApi {

    companion object {
        const val BASE_URL = "https://api.bitbucket.org/2.0/"
    }

    @GET("repositories?fields=values.name,values.owner,values.description")
    fun getRepositories(): Observable<BitbucketRepository>
}