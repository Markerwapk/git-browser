package eu.softmark.gitbrowser.utils

import eu.softmark.gitbrowser.mappers.BitbucketMapper
import eu.softmark.gitbrowser.mappers.GithubMapper
import eu.softmark.gitbrowser.model.RepositoryItem
import eu.softmark.gitbrowser.webapi.BitbucketApi
import eu.softmark.gitbrowser.webapi.GithubApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException


class GitRepositoryCache(private val githubApi: GithubApi,
                         private val bitbucketApi: BitbucketApi,
                         private val githubMapper: GithubMapper,
                         private val bitbucketMapper: BitbucketMapper) {

    companion object {
        private const val STATE_UNKNOWN = 0
        private const val STATE_IN_PROGRESS = 1
        private const val STATE_FINISHED = 2
        private const val STATE_ERROR = 3
    }

    interface GitRepositoryListener {
        fun onRefreshing()
        fun onFinish()
        fun onError(throwable: Throwable)
    }

    private var state = STATE_UNKNOWN
    private val listeners = mutableListOf<GitRepositoryListener>()

    val repositoryItems = mutableListOf<RepositoryItem>()

    fun register(listener: GitRepositoryListener) {
        listeners.add(listener)
    }

    fun refreshData() {
        if (state == STATE_IN_PROGRESS) {
            return
        }

        state = STATE_IN_PROGRESS
        notifyListeners()

        Observable.zip(listOf(
                getGithubApiObservable(),
                getBitbucketApiObservable())) { items ->
            repositoryItems.clear()

            items.forEach { item ->
                (item as? List<*>)?.mapNotNull {
                    it as? RepositoryItem
                }?.let {
                    repositoryItems.addAll(it)
                }
            }
        }.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    state = STATE_FINISHED
                    notifyListeners()
                }, {
                    if (repositoryItems.size > 0 && it is IOException) {
                        state = STATE_FINISHED
                        notifyListeners()
                    } else {
                        state = STATE_ERROR
                        notifyListenersAboutError(it)
                    }
                })
    }

    private fun getBitbucketApiObservable(): Observable<List<RepositoryItem>> =
            bitbucketApi.getRepositories().map { repositoryItems ->
                repositoryItems.bitbucketValues.map { bitbucketMapper.mapToLocal(it) }
            }

    private fun getGithubApiObservable(): Observable<List<RepositoryItem>> =
            githubApi.getRepositories().map { repositoryItems ->
                repositoryItems.map { githubMapper.mapToLocal(it) }
            }

    private fun notifyListeners() {
        listeners.forEach { notifyListener(it) }
    }


    private fun notifyListener(listener: GitRepositoryListener) {
        when (state) {
            STATE_IN_PROGRESS -> listener.onRefreshing()
            STATE_FINISHED -> listener.onFinish()
        }
    }

    private fun notifyListenersAboutError(throwable: Throwable) {
        listeners.forEach {
            notifyListenerAboutError(it, throwable)
        }
    }

    private fun notifyListenerAboutError(listener: GitRepositoryListener, throwable: Throwable) {
        listener.onError(throwable)
    }

    fun unregister(listener: GitRepositoryListener) {
        listeners.remove(listener)
    }
}