package eu.softmark.gitbrowser

import android.app.Application
import eu.softmark.gitbrowser.components.AppComponent
import eu.softmark.gitbrowser.components.DaggerAppComponent
import eu.softmark.gitbrowser.modules.AppModule


class App : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}