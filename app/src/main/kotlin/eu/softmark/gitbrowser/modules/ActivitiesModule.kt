package eu.softmark.gitbrowser.modules

import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import eu.softmark.gitbrowser.activities.repository.list.RepositoriesActivityPresenter
import eu.softmark.gitbrowser.activities.repository.list.RepositoriesAdapter
import eu.softmark.gitbrowser.utils.GitRepositoryCache

@Module
class ActivitiesModule {

    @Provides
    fun provideRepositoriesAdapter(picasso: Picasso): RepositoriesAdapter {
        return RepositoriesAdapter(picasso)
    }

    @Provides
    fun provideRepositoriesActivityPresenter(gitRepositoryCache: GitRepositoryCache): RepositoriesActivityPresenter {
        return RepositoriesActivityPresenter(gitRepositoryCache)
    }
}