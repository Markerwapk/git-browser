package eu.softmark.gitbrowser.modules

import dagger.Module
import dagger.Provides
import eu.softmark.gitbrowser.mappers.BitbucketMapper
import eu.softmark.gitbrowser.mappers.GithubMapper
import javax.inject.Singleton

@Module
class MapperModule {

    @Provides
    @Singleton
    fun provideGithubMapper(): GithubMapper {
        return GithubMapper()
    }

    @Provides
    @Singleton
    fun provideBitbucketMapper(): BitbucketMapper {
        return BitbucketMapper()
    }
}