package eu.softmark.gitbrowser.modules

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import eu.softmark.gitbrowser.BuildConfig
import eu.softmark.gitbrowser.webapi.BitbucketApi
import eu.softmark.gitbrowser.webapi.GithubApi
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ApiModule {
    companion object {
        private const val TIMEOUT_IN_SECONDS = 20L
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }

    @Provides
    fun provideOkHttpCache(context: Context): Cache {
        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        return Cache(context.cacheDir, cacheSize.toLong())
    }

    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return httpLoggingInterceptor
    }

    @Provides
    fun provideBaseOkHttpClient(cache: Cache,
                                httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient {

        return OkHttpClient.Builder()
                .readTimeout(TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)
                .cache(cache)
                .addInterceptor(httpLoggingInterceptor).build()
    }

    @Provides
    fun provideRxJava2CallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

    @Provides
    fun provideBaseRetrofitBuilder(okHttpClient: OkHttpClient,
                                   gsonConverterFactory: GsonConverterFactory,
                                   callAdapter: RxJava2CallAdapterFactory): Retrofit.Builder {
        return Retrofit
                .Builder()
                .client(okHttpClient)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(callAdapter)
    }

    @Provides
    fun provideGithubApi(retrofitBuilder: Retrofit.Builder): GithubApi {
        return retrofitBuilder.baseUrl(GithubApi.BASE_URL).build().create(GithubApi::class.java)
    }

    @Provides
    fun provideBitbucketApi(retrofitBuilder: Retrofit.Builder): BitbucketApi {
        return retrofitBuilder.baseUrl(BitbucketApi.BASE_URL).build().create(BitbucketApi::class.java)
    }
}