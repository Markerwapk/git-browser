package eu.softmark.gitbrowser.modules

import android.content.Context
import android.os.Handler
import android.os.Looper
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = context

    @Provides
    @Singleton
    fun provideHandler(): Handler = Handler(Looper.getMainLooper())

    @Provides
    fun providePicasso(): Picasso = Picasso.get()
}