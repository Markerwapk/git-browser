package eu.softmark.gitbrowser.modules

import dagger.Module
import dagger.Provides
import eu.softmark.gitbrowser.mappers.BitbucketMapper
import eu.softmark.gitbrowser.mappers.GithubMapper
import eu.softmark.gitbrowser.utils.GitRepositoryCache
import eu.softmark.gitbrowser.webapi.BitbucketApi
import eu.softmark.gitbrowser.webapi.GithubApi
import javax.inject.Singleton

@Module
class UtilsModule {
    @Provides
    @Singleton
    fun provideGitRepositoryCache(githubApi: GithubApi,
                                  bitbucketApi: BitbucketApi,
                                  githubMapper: GithubMapper,
                                  bitbucketMapper: BitbucketMapper): GitRepositoryCache {
        return GitRepositoryCache(
                githubApi,
                bitbucketApi,
                githubMapper,
                bitbucketMapper)
    }
}