package eu.softmark.gitbrowser.mappers


interface ApiMapper<LOCAL, API> {
    fun mapToLocal(apiModel: API): LOCAL
}