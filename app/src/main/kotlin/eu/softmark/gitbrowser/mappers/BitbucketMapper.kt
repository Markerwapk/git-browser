package eu.softmark.gitbrowser.mappers

import eu.softmark.gitbrowser.model.bitbucket.BitbucketRepositoryItem
import eu.softmark.gitbrowser.model.bitbucket.BitbucketValue


class BitbucketMapper : ApiMapper<BitbucketRepositoryItem, BitbucketValue> {
    override fun mapToLocal(apiModel: BitbucketValue): BitbucketRepositoryItem {
        return BitbucketRepositoryItem(
                apiModel.name,
                apiModel.bitbucketOwner.displayName,
                apiModel.bitbucketOwner.bitbucketLinks.bitbucketAvatar.href,
                apiModel.description)
    }
}