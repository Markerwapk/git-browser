package eu.softmark.gitbrowser.mappers

import eu.softmark.gitbrowser.model.gitbub.GitHubRepository
import eu.softmark.gitbrowser.model.gitbub.GithubRepositoryItem


class GithubMapper : ApiMapper<GithubRepositoryItem, GitHubRepository> {
    override fun mapToLocal(apiModel: GitHubRepository): GithubRepositoryItem {
        return GithubRepositoryItem(
                apiModel.name,
                apiModel.githubOwner.login,
                apiModel.githubOwner.avatarUrl,
                apiModel.description)
    }

}